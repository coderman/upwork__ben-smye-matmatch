<?php
// This theme requires WordPress 5.3 or later.
if ( version_compare( $GLOBALS['wp_version'], '5.4', '<' ) ) {
  require get_template_directory() . '/inc/back-compat.php';
}

// Autoload composer files
require_once( __DIR__ . '/vendor/autoload.php' );

// Initialize timber
$timber = new Timber\Timber();

// Enable featured images
add_theme_support( 'post-thumbnails' );

// Load ACF configuration
include 'config/acf/config.php';

// Initialize ACF Pro options
if( function_exists('acf_add_options_page') ) {
  acf_add_options_page();
}

// Enqueue assets
function theme_files() {
  wp_enqueue_style('style_base',
    get_theme_file_uri('assets/css/styles.css'),
    [],
    filemtime(get_template_directory() . '/assets/css/styles.css'),
    false
  );

  // wp_enqueue_style('add_google_fonts', 'https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700&display=swap');

  // Custom JS files.
  // Global JS - every page
  wp_enqueue_script( 'script_main', get_template_directory_uri() . '/assets/js/main.js', array(), filemtime(get_template_directory() . '/assets/js/main.js'), true );

  // Search results
  // wp_enqueue_script( 'script_search', get_template_directory_uri() . '/assets/js/search.js', array(), filemtime(get_template_directory() . '/assets/js/search.js'), true );

  // Learn page
  if (is_page('learn')) {
    wp_enqueue_script( 'script_learn', get_template_directory_uri() . '/assets/js/learn.js', array(), filemtime(get_template_directory() . '/assets/js/learn.js'), true );
  // Use-Cases page
  } else if (is_page('use-cases')) {
    wp_enqueue_script( 'script_use_cases', get_template_directory_uri() . '/assets/js/use-cases.js', array(), filemtime(get_template_directory() . '/assets/js/use-cases.js'), true );
  } else if (is_page('homepage')) {
    // Homepage
    wp_enqueue_script( 'script_homepage', get_template_directory_uri() . '/assets/js/homepage.js', array(), filemtime(get_template_directory() . '/assets/js/homepage.js'), true );
  }
}

add_action('wp_enqueue_scripts', 'theme_files');

// Global settings (site-wide)
add_filter( 'timber_context', 'coderman_timber_context' );

function coderman_timber_context( $context ) {
  // Global options
  $context['options'] = get_fields('option');
  $context['currentYear'] = date("Y");
  $context['menu'] = [
    'header' => new Timber\Menu( 'header' ),
    'hero' => new Timber\Menu( 'hero' ),
    'footer' => new Timber\Menu( 'footer' ),
  ];

  return $context;
}

// Helper functions
function initials($name) {
  $words = explode(' ', $name);
  $first = substr($name, 0, 1) ? substr($name, 0, 1) : '';
  $second = '';

  if (isset($words[1])) {
    $second = substr($words[1], 0, 1);
  }

  return $first.$second;

}
