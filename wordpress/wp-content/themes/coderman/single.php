<?php


  $context = Timber::context();
  $context['post'] = new Timber\Post();

  $postID = get_the_ID();

  $categoryID = wp_get_post_categories($postID)[0];

  $context['category'] = [
    'name' => get_category( $categoryID )->name,
    'url' => get_category_link($categoryID)
  ];

  $context['related'] = Timber::get_posts([
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => '4',
    'category' => $categoryID,
    'post__not_in' => array(get_the_ID())
  ]);

  $context['breadcrumbs'] = new Timber\Menu( 'post-breadcrumbs' );

  Timber::render( 'templates/single.twig', $context);
