<?php

$context = Timber::context();

$page = get_page_by_title( '404' );
$context['content'] = apply_filters('the_content', $page->post_content);
$site = new TimberSite();

Timber::render( 'templates/404.twig', $context );
