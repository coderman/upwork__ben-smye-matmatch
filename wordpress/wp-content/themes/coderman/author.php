<?php

  $context = Timber::context();
  $context['post'] = new Timber\Post();

  $authorId = $context['post']->author->ID;

  $context['posts'] = Timber::get_posts([
    'author' =>  $authorId,
  ]);

  // print_r('<pre>');
  // var_dump();
  // print_r('</pre>');

  Timber::render( 'templates/author.twig', $context);
