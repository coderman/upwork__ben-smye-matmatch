exports.Settings = {
  webpack: {
    rules: [
      {
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  targets: {
                    ie: '11'
                  }
                }
              ],
              [
                '@babel/preset-react'
              ]
            ]
          }
        },
        resolve: {
          extensions: [".js", ".jsx"]
        },
      }
    ],
    target: 'web'
  },
  path: {
    src: './production',
    dest: '../assets'
  }
};
