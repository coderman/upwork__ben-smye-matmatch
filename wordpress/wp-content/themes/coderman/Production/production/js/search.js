import React from 'react';
import ReactDOM from 'react-dom';
import SearchResults from './Components/SearchResults/SearchResults';

const results = document.querySelector('#search-results');

if (results) {
  ReactDOM.render(
    <SearchResults
      term={ results.getAttribute('data-term')}
      home={ results.getAttribute('data-home')}
      squidexLimit={ results.getAttribute('data-squidex-limit') }
      wpLimit={ results.getAttribute('data-wp-limit') }
      squidexApi={ results.getAttribute('data-squidex-api-url') }
      skeletons={ results.getAttribute('data-skeletons') }
    />,
  results);
}