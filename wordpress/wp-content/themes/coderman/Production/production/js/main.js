import { disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock';
import 'lazysizes';

// Sub-menu toggle (mobile)
const submenuToggles = document.querySelectorAll('.js-dropdown-section-title');

submenuToggles.forEach( $subToggle => {
  $subToggle.addEventListener('click', e => {
    const target = e.currentTarget;

    target.classList.toggle('is-open');

    target.nextElementSibling.classList.toggle('is-open');
  });
});

// Nav toggle
const toggle = document.querySelector('.js-toggle');
const nav = document.querySelector('.js-nav-header');

toggle.addEventListener('click', e => {
  toggle.classList.toggle('is-open');
  nav.classList.toggle('is-open');

  if (toggle.classList.contains('is-open')) {
    disableBodyScroll(nav);
  } else {
    enableBodyScroll(nav);
  }
});

// Modal
const modal = document.getElementById("modal");

const btn = document.querySelector('.js-modal-open') ? document.querySelector('.js-modal-open') : {};

const span = document.getElementsByClassName("js-modal-close")[0] ? document.getElementsByClassName("js-modal-close")[0] : {};

btn.onclick = function() {
  modal.style.display = "block";
  disableBodyScroll(modal);
}

span.onclick = function() {
  modal.style.display = "none";
  clearAllBodyScrollLocks();
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
    clearAllBodyScrollLocks();
  }
}
