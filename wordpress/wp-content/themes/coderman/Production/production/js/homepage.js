import React, { useState, useEffect, Component } from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import CONST from './modules/const';
import { truncate } from './modules/utils';

import Author from './Components/Author/Author';

// Squidex render elements
const squidex = {
  api_url: document.querySelector('.js-squidex-data').getAttribute('data-squidex-api'),
  learn: document.querySelector('#squidex-learn'),
  cases: document.querySelector('#squidex-use-cases'),
  top: document.querySelector('#squidex-top-rated'),
};

// Homepage: Top Read
if (squidex.top) {
  const Article = props => {
    const loading = props.loaded ? '' : 'is-loading';

    return (
      <a className="card-link" href={props.url}>
        <div className={`card card--top-read card--ajax ${loading}`}>
          <img src={props.img} className="img-responsive card__img" width="360" height="238"/>
          <dl className="card__icon">
            <dt>{ props.icon  }</dt>
            <dd>{ props.type }</dd>
          </dl>
          <h4>{props.title}</h4>
          <p>{ truncate(props.content, 120) }</p>
          <Author author={props.author} />
        </div>
      </a>
    );
  };

  class TopArticles extends Component {
    constructor() {
      super();

      this.state = {
        loaded: false,
        items: [
          { type: '', body: ''}, { type: '', body: ''}
        ]
      }

      this.icons = {
        lightbulb: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 352 512"><path fill="currentColor" d="M176 80c-52.94 0-96 43.06-96 96 0 8.84 7.16 16 16 16s16-7.16 16-16c0-35.3 28.72-64 64-64 8.84 0 16-7.16 16-16s-7.16-16-16-16zM96.06 459.17c0 3.15.93 6.22 2.68 8.84l24.51 36.84c2.97 4.46 7.97 7.14 13.32 7.14h78.85c5.36 0 10.36-2.68 13.32-7.14l24.51-36.84c1.74-2.62 2.67-5.7 2.68-8.84l.05-43.18H96.02l.04 43.18zM176 0C73.72 0 0 82.97 0 176c0 44.37 16.45 84.85 43.56 115.78 16.64 18.99 42.74 58.8 52.42 92.16v.06h48v-.12c-.01-4.77-.72-9.51-2.15-14.07-5.59-17.81-22.82-64.77-62.17-109.67-20.54-23.43-31.52-53.15-31.61-84.14-.2-73.64 59.67-128 127.95-128 70.58 0 128 57.42 128 128 0 30.97-11.24 60.85-31.65 84.14-39.11 44.61-56.42 91.47-62.1 109.46a47.507 47.507 0 00-2.22 14.3v.1h48v-.05c9.68-33.37 35.78-73.18 52.42-92.16C335.55 260.85 352 220.37 352 176 352 78.8 273.2 0 176 0z"/></svg>,
        graduation: <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M622.34 153.2L343.4 67.5c-15.2-4.67-31.6-4.67-46.79 0L17.66 153.2c-23.54 7.23-23.54 38.36 0 45.59l48.63 14.94c-10.67 13.19-17.23 29.28-17.88 46.9C38.78 266.15 32 276.11 32 288c0 10.78 5.68 19.85 13.86 25.65L20.33 428.53C18.11 438.52 25.71 448 35.94 448h56.11c10.24 0 17.84-9.48 15.62-19.47L82.14 313.65C90.32 307.85 96 298.78 96 288c0-11.57-6.47-21.25-15.66-26.87.76-15.02 8.44-28.3 20.69-36.72L296.6 284.5c9.06 2.78 26.44 6.25 46.79 0l278.95-85.7c23.55-7.24 23.55-38.36 0-45.6zM352.79 315.09c-28.53 8.76-52.84 3.92-65.59 0l-145.02-44.55L128 384c0 35.35 85.96 64 192 64s192-28.65 192-64l-14.18-113.47-145.03 44.56z"/></svg>
      }
    }

    componentDidMount() {
      axios.get(squidex.api_url)
        .then(response => {
          // handle success
          const newState = { ...this.state };
          newState.items[0].type = 'Learn';
          newState.items[0].body = response.data['learn'][0];
          newState.items[1].type = 'Use case';
          newState.items[1].body = response.data['use-cases'][0];
          newState.loaded = true;
          this.setState(newState);
        })
        .catch(function (error) {
          // handle error
        })
        .then(function () {
          // always executed
        });
    }

    getIcon (type) {
      if (type === 'Learn') {
        return this.icons.graduation;
      } else if (type === 'Use case') {
        return this.icons.lightbulb;
      }
    };

    render () {
      const content = this.state.items.map((item, index) => {

        return (
          <div className="col-sm-6" key={index}>
            <Article
              url={item.body.url}
              img={item.body.image}
              title={item.body.title}
              loaded={this.state.loaded}
              author={item.body.author}
              content={item.body.description}
              type={item.type}
              icon={this.getIcon(item.type)}
              ></Article>
          </div>
        );
      });

      return (
        <div className="row">
          { content }
        </div>
      );
    }
  }

  ReactDOM.render(<TopArticles />, squidex.top);
}

// Homepage: Learn
if (squidex.learn) {
  const Article = props => {
    const loading = props.loaded ? '' : 'is-loading';

    return (
      <a href={props.url} className="card-link">
        <article className={`card card--aside card--ajax ${loading}`}>
          <img src={ props.img } className="img-responsive card__img card__img--aside" width="132" height="100"/>
          <div className="card__content card__content--aside">
            <h6>{props.title}</h6>
            <p>{truncate(props.content, 120)}</p>
          </div>
        </article>
      </a>
    );
  };

  class Learn extends Component {
    constructor() {
      super();

      this.state = {
        loaded: false,
        articles: [
          {},{},{},{},{}
        ]
      }
    }

    componentDidMount() {
      axios.get(squidex.api_url)
        .then(response => {
          // handle success
          const newState = {...this.state};
          newState.articles = response.data.learn;
          newState.loaded = true;
          this.setState(newState);
        })
        .catch(function (error) {
          // handle error
        })
        .then(function () {
          // always executed
        });
    }

    render () {
      const content = this.state.articles.map((item, index) => {
        return (
          <li key={index}>
            <Article
              img={item.image}
              loaded={this.state.loaded}
              title={item.title}
              url={item.url}
              content={item.description}
            ></Article>
          </li>
        );
      });

      return (
        <>
          <h2>Learn</h2>
          <ul className="list-reset card-list">
            { content }
          </ul>
        </>
      );
    }
  }

  ReactDOM.render(<Learn />, squidex.learn);
}

// Homepage: Use Cases
if (squidex.cases) {
  const Article = props => {
    const loading = props.loaded ? '' : 'is-loading';

    return (
      <div className="col-xs-6 col-sm-4 col-md-3">
        <a className="card-link" href={props.url}>
          <div className={`card card--ajax ${loading}`}>
            <img src={props.img} className="img-responsive card__img" width="263" height="176"/>
            <h6>{props.title}</h6>
            <Author author={props.author} />
          </div>
        </a>
      </div>
    );
  };

  class Cases extends Component {
    constructor() {
      super();

      this.state = {
        loaded: false,
        cases: [
          {},{},{},{}
        ]
      }
    }

    componentDidMount() {
      axios.get(squidex.api_url)
        .then(response => {
          // handle success
          const newState = { ...this.state };

          // Load maximum of 4 artiles even if there are more.
          newState.cases = response.data['use-cases'].splice(0, 4);

          newState.loaded = true;
          this.setState(newState);
        })
        .catch(function (error) {
          // handle error
        })
        .then(function () {
          // always executed
        });
    }

    render () {
      const content = this.state.cases.map((item, index) => {
        return (
          <Article
            key={index}
            url={item.url}
            img={item.image}
            title={item.title}
            loaded={this.state.loaded}
            author={item.author}
            content={item.description}
            ></Article>
        );
      });
      return (
        <>
          <h2>Use Cases</h2>
          <div className="row row--vertical">
            { content }
          </div>
        </>
      );
    }
  }

  ReactDOM.render(<Cases />, squidex.cases);
}
