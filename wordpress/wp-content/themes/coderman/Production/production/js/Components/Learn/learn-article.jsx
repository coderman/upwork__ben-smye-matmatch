import React from 'react';

import Tags from '../Tags/tags';
import Image from '../Image/Image';
import Time from '../Time/Time';

const LearnArticle = props => {

  const loading = props.loaded ? '' : 'is-loading';

  return (
    <div className="card-link">
      <div className={`card card--featured card--ajax ${loading}`}>
        <div className="row">
          <div className="col-md-6">
            <a href={props.url}>
              <Image
                image={props.img}
                classes="img-responsive card__img card__img--featured"
                width="263"
                height="160"
                />
            </a>
          </div>
          <div className="col-md-6">
            <div className="card__content">
              <a className="card-link card-link--static" href={props.url}>
                <h4 className="mb-05x card__title">{ props.title }</h4>
              </a>
              <p className="card__description">{ props.content }</p>
              <Time time={props.time} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LearnArticle;
