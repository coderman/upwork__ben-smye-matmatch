import React, { Component } from 'react';
import axios from 'axios';
import Result from './Result';

const List = props => {
  return (
    <ul className="list-reset app__posts">
      {
        props.results.map((result, index) => {
          return (
            <li key={index} data-index={index}>
              <Result
                title={result.title}
                content={result.description ? result.description : null}
                url={result.url}
                src={result.image}
                loaded={props.loaded}
              />
            </li>
          );
        })
      }
    </ul>
  );
};

class SearchResults extends Component {

  constructor(props) {
    super(props);

    this.term = this.props.term;
    this.home = this.props.home;
    this.squidexApi = this.props.squidexApi;
    this.wpLimit = this.props.wpLimit;
    this.squidexLimit = this.props.squidexLimit;

    this.state = {
      // Ids of post's featured media
      wpMedia: [],
      resultsStage: [],
      results: Array(parseInt(this.props.skeletons)).fill({}),
      loaded: false,
    };
  }

  // Get post data from Wordpress
  getPostData(results) {
    const postIds = results.map(result => result.id);

    return new Promise((resolve, reject) => {
      axios.get(`${this.home}/wp-json/wp/v2/posts?include=${postIds.toString()}`)
        .then(response => {

          const data = response.data;

          // First item in the results should be the last. Remove it from the array.
          const misplacedItem = data.shift();

          // Apend the misplaced item to the end of the array.
          data.push(misplacedItem);

          if (results.length === 0) {
            resolve([]);
          }

          resolve(data);
        })
    });
  }

  getPostFeaturedImage(medias) {
    const postIds = medias;

    // console.log(`${this.home}/wp-json/wp/v2/media?include=${postIds.toString()}`);

    return new Promise((resolve, reject) => {
      axios.get(`${this.home}/wp-json/wp/v2/media?include=${postIds.toString()}`)
        .then(response => {
          resolve(response.data);
        })
    });
  }

  getSquidexData() {
    // ONLY UNTIL CORS GETS SORTED
    // this.setState({
    //   results: [...this.state.resultsStage],
    //   loaded: true,
    // }, () => console.log('state', this.state))
    // ONLY UNTIL CORS GETS SORTED ./end

    return new Promise((resolve, reject) => {
      axios.get(`${this.squidexApi}?q=${this.term}&skip=0&limit=${this.squidexLimit}`)
        .then(response => {
          console.log(`all ok`, response);
          resolve(reponse.data);
        })
    });
  }

  /**
   * Take the posts from stage (provided by WP) and combine them with
   * results from 'Squidex'.
   * @param  {Array}  data Results from `Squidex` search.
   */
  fillResults (data = []) {
    this.setState({
      results: [...this.state.resultsStage, ...data],
      loaded: true,
    })
  }

  componentDidMount() {
    axios.get(`${this.home}/wp-json/wp/v2/search?search=${this.term}&per_page=${this.wpLimit}`)
      .then(response => {
        // List of raw search results.
        const results = response.data;

        this.getPostData(results)
          .then(posts => {
            this.setState({
              wpMedia: posts.map(post => post.featured_media),
              resultsStage: posts.length ? (
                  posts.map(post => { return {
                    id: post.id,
                    title: post.title.rendered,
                    description: post.excerpt.rendered,
                    url: post.link,
                    image: '',
                  }})
                ) : []
            }, () => {
              this.getPostFeaturedImage(this.state.wpMedia)
              .then(featured => {

                const newState = {...this.state};

                // Match correct featured image to its correct post.
                featured.forEach(featured => {
                  newState.resultsStage.map(post => {
                    if (post.id === featured.post) {
                      return post.image = featured.media_details.sizes.medium.source_url;
                    }
                  });
                });

                this.setState(
                  newState,
                  () => this.getSquidexData()
                    .then(results  => {
                      this.fillResults(results);
                    })
                );
              })
            })
          })
      })
      .catch(function (error) {
        // handle error
        this.getSquidexData();
      })
      .then(function () {
        // always executed
      });
  }

  render() {
    if (this.state.results.length) {
      return <List results={this.state.results} loaded={this.state.loaded}></List>
    }

    return <p>No results found :/</p>
  }
}

export default SearchResults;
