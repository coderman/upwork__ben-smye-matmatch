import React from 'react';

const ImagePreloader = props => {
  return (
    <div className="img-loader">
      {
        props.src ? (
          <img src={props.src} alt={props.alt} className="img-responsive"/>
        ) : null
      }
    </div>
  );
};

export default ImagePreloader;
