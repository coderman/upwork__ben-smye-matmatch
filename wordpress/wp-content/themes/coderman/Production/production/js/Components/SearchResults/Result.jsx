import React from 'react';
import Tags from '../Tags/tags';
import Image from '../Image/Image';
import Time from '../Time/Time';
import ImageLoader from '../Image/ImageLoader';

const Result = props => {

  return (
    props.loaded ? (
      <a href={props.url} className="card-link">
        <div className='card card--ajax'>
          <div className="row">
            <div className="app__article-img app__article-img--lg">
              <ImageLoader src={ props.src }/>
            </div>
            <div className="app__article-content app__article-content--lg">
              <div className="card__content">
                <h4 className="mb-05x card__title"
                  dangerouslySetInnerHTML={{ __html: props.title }}></h4>
                <Tags links={ props.tags }  />
                <div
                  className="card__description"
                  dangerouslySetInnerHTML={{ __html: props.content }}></div>
                <Time time={props.time} />
              </div>
            </div>
          </div>
        </div>
      </a>
    ) : <div className="skeleton skeleton--result"></div>
  );
};

export default Result;
