import React from 'react';

const AppHero = props => {

  return (
    <div className={`app__hero ${props.loaded ? null : 'is-loading'}`}>
      <h1 className="app__title">{ props.title }</h1>
      <img
        onLoad={ props.onLoad }
        className="img-responsive mb-0x app__hero-img"
        width="945"
        height="174"
        src={ props.src }
        alt="Background image"
      />
    </div>

  );
};

export default AppHero;
