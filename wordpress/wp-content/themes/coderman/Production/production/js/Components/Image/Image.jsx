import React from 'react';

const Image = props => {
  const img = props.image ? (
    <img
      onLoad={props.onLoad}
      srcSet={
        `${props.image[0]}, ${props.image[1]} 2x`
      }
      src={props.image[0]}
      className={props.classes}
      width={props.width}
      height={props.height}
      id={props.id}
    />
  ) : null;

  return img;
};

export default Image;
