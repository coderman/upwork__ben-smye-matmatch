import React from 'react';

const MenuAside = props => {

  const { items, activeMenu, activeSub, onClick } = props;

  // Find index of the currently active menu.
  const menuIndex = items.findIndex(item => item.id === activeMenu);

  const list = items[menuIndex].level_1.map((item, index) => {
    return (
      <li key={index}
          className={item.id === activeSub ? 'active' : null}>
        <a href={item.url} id={item.id} onClick={onClick}>{item.title}</a>
      </li>
    );
  });

  return (
    <aside className="app__aside">
      <nav>
        <ul className="app__nav-aside list-reset">
          {list}
        </ul>
      </nav>
    </aside>
  );
};

export default MenuAside;
