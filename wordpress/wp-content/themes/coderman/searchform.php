<?php

$context = Timber::context();

$site = new TimberSite();
$context['value'] = get_search_query();

Timber::render( 'templates/search-form.twig', $context );
