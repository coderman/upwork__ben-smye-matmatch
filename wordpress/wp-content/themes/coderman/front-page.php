<?php

$context = Timber::context();
$post = new Timber\Post();


$context['hero'] = [
  'title' => get_field('hero')['title'],
  'content' => get_field('hero')['content'],
  'cta' => get_field('hero')['link'],
  'bg' => get_field('hero')['background'],
];

$context['squidex_api'] = get_field('squidex_api_url');

// Top reads
$context['top'] = [
  'title' => get_field('top_reads'),
  'post' => new Timber\Post(get_field('top_read_article')->ID)
];

$context['latest'] = [
  'title' => get_field('latest'),
  'list' => Timber::get_posts([
    'post_type' => 'post',
    'post_status' => 'publish',
    'posts_per_page' => '4'
  ])
];

Timber::render( 'templates/front-page.twig', $context );
