<?php

$context = Timber::context();

$post = new Timber\Post();

$context['term'] = esc_attr( get_search_query( false ) );
$context['home'] = site_url();

$squidex_api = get_fields('option')['search']['squidex_search_api'];
$squidex_limit = get_fields('option')['search']['squidex_results'];

if (!empty($context['term'])) {
  // Fetch 'Squidex' search results.
  $squidex_data = json_decode(
    @file_get_contents($squidex_api."?q=".$context['term']."&skip=0&limit=".$squidex_limit),
    TRUE);

  $results = [
    'wordpress' => @array_map(function($post) {
      $id = $post->id;

      return [
        'url' => get_permalink($id),
        'image' => get_the_post_thumbnail_url($id),
        'created' => strtotime($post->post_date),
        'title' => $post->post_title,
        'type' => get_fields('option')['search']['tag_for_wordpress_results'],
        'description' => $post->post_content,
      ];
    }, $wp_query->posts),
    'squidex' => @array_map(function($post) {
      $post['created'] = strtotime($post['created']);

      return $post;
    }, $squidex_data),
  ];

  // Combine 'Wordpress' and 'Squidex' results.
  $context['results'] = @array_merge($results['wordpress'], $results['squidex']);

  // Sort the search results by their dates (descending) - UNIX time.
  @array_multisort(array_column($context['results'], 'created'), SORT_DESC, $context['results']);
} else {
  $context['empty'] = true;
}


Timber::render( 'templates/search-results.twig', $context );
